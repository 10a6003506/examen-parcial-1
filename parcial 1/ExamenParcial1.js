var url = 'http://jsonplaceholder.typicode.com/todos'

// Función para obtener todos los pendientes de la API
function ObtenerTodos() {
  return fetch(url)
    .then(response => response.json());
}

// Función para filtrar los pendientes por su estado
function FiltrarPorEstado(todos, status) {
  return todos.filter(todo => todo.completed === status);
}

// Función para mostrar la lista de IDs
function MostrarIDs(todos) {
  console.log(todos.map(todo => todo.id));
}

// Función para mostrar la lista de IDs y títulos
function MostrarIDsYTitulos(todos) {
  console.log(todos.map(todo => ({ id: todo.id, title: todo.title })));
}

// Función para mostrar la lista de IDs y userID
function MostrarIDsYUsuario(todos) {
  console.log(todos.map(todo => ({ id: todo.id, userId: todo.userId })));
}

// Menú interactivo
function MostrarMenu() {
  let inquirerModule;
  import('inquirer')
    .then(module => {
      inquirerModule = module.default || module;
    });

  ObtenerTodos().then(todos => {
    inquirerModule.prompt([
      {
        type: 'list',
        name: 'action',
        message: '¿Qué deseas ver?',
        choices: [
          'Lista de todos los pendientes (solo IDs)',
          'Lista de todos los pendientes (IDs y Títles)',
          'Lista de todos los pendientes sin resolver (ID y Title)',
          'Lista de todos los pendientes resueltos (ID y Title)',
          'Lista de todos los pendientes (IDs y userID)',
          'Lista de todos los pendientes resueltos (ID y userID)',
          'Lista de todos los pendientes sin resolver (ID y userID)',
          'Salir'
        ]
      }
    ]).then(({ action }) => {
      switch (action) {
        case 'Lista de todos los pendientes (solo IDs)':
          MostrarIDs(todos);
          break;
        case 'Lista de todos los pendientes (IDs y Títles)':
          MostrarIDsYTitulos(todos);
          break;
        case 'Lista de todos los pendientes sin resolver (ID y Title)':
          MostrarIDsYTitulos(FiltrarPorEstado(todos, false));
          break;
        case 'Lista de todos los pendientes resueltos (ID y Title)':
          MostrarIDsYTitulos(FiltrarPorEstado(todos, true));
          break;
        case 'Lista de todos los pendientes (IDs y userID)':
          MostrarIDsYUsuario(todos);
          break;
        case 'Lista de todos los pendientes resueltos (ID y userID)':
          MostrarIDsYUsuario(FiltrarPorEstado(todos, true));
          break;
        case 'Lista de todos los pendientes sin resolver (ID y userID)':
          MostrarIDsYUsuario(FiltrarPorEstado(todos, false));
          break;
        case 'Salir':
          return;
      }

      // Vuelve a mostrar el menú después de cada acción
      MostrarMenu();
    });
  }).catch(console.error);
}

// Ejecuta el menú al iniciar la aplicación
MostrarMenu();
