self.addEventListener('fetch', event => {
    if (event.request.url.includes('http://127.0.0.1:8080/img/LogoFalso.jpeg')) {
        event.respondWith(
            fetch('http://127.0.0.1:8080/img/LogoOriginal.png')
        );
    } 
    else if (event.request.url.includes('http://jsonplaceholder.typicode.com/todos')) {
        event.respondWith(
            fetch(event.request).then(response => {
                const clonedResponse = response.clone();
                if (event.request.method === 'GET') {
                    return response.json().then(data => {
                        const modifiedData = data.map(todo => ({
                            id: todo.id + '♦️', 
                            title: todo.title,
                            userId: todo.userId,
                            completed: todo.completed
                        }));
                        return new Response(JSON.stringify(modifiedData), {
                            headers: response.headers,
                            status: response.status,
                            statusText: response.statusText
                        });
                    });
                } else {
                    return clonedResponse;
                }
            })
        );
    }
});

self.addEventListener('message', event => {
    if (event.data && event.data.action === 'obtenerPendientes') {
        event.respondWith(
            fetch('http://jsonplaceholder.typicode.com/todos')
                .then(response => response.json())
                .catch(error => console.error(error))
        );
    } else if (event.data && event.data.action === 'obtenerPendientesSinResolver') {
        event.respondWith(
            fetch('http://jsonplaceholder.typicode.com/todos?completed=false')
                .then(response => response.json())
                .catch(error => console.error(error))
        );
    } else if (event.data && event.data.action === 'obtenerPendientesResueltos') {
        event.respondWith(
            fetch('http://jsonplaceholder.typicode.com/todos?completed=true')
                .then(response => response.json())
                .catch(error => console.error(error))
        );
    } else if (event.data && event.data.action === 'obtenerPendientesConUserID') {
        event.respondWith(
            fetch('http://jsonplaceholder.typicode.com/todos')
                .then(response => response.json())
                .catch(error => console.error(error))
        );
    } else if (event.data && event.data.action === 'obtenerPendientesResueltosConUserID') {
        event.respondWith(
            fetch('http://jsonplaceholder.typicode.com/todos?completed=true')
                .then(response => response.json())
                .catch(error => console.error(error))
        );
    }
});
