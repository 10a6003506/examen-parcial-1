if (navigator.serviceWorker) {
    console.log("Si es compatible"); 
    navigator.serviceWorker.register('/sw.js').then(registration => {
        const sendMessageToServiceWorker = message => {
            return new Promise((resolve, reject) => {
                const messageChannel = new MessageChannel();
                messageChannel.port1.onmessage = event => {
                    if (event.data.error) {
                        reject(event.data.error);
                    } else {
                        resolve(event.data);
                    }
                };
                registration.active.postMessage(message, [messageChannel.port2]);
            });
        };

        function obtenerPendientes() {
            sendMessageToServiceWorker({ action: 'obtenerPendientes' })
                .then(data => mostrarSalida(data))
                .catch(error => console.error(error));
        }

        function obtenerPendientesSinResolver() {
            sendMessageToServiceWorker({ action: 'obtenerPendientesSinResolver' })
                .then(data => mostrarSalida(data))
                .catch(error => console.error(error));
        }

        function obtenerPendientesResueltos() {
            sendMessageToServiceWorker({ action: 'obtenerPendientesResueltos' })
                .then(data => mostrarSalida(data))
                .catch(error => console.error(error));
        }

        function obtenerPendientesConUserID() {
            sendMessageToServiceWorker({ action: 'obtenerPendientesConUserID' })
                .then(data => mostrarSalida(data))
                .catch(error => console.error(error));
        }

        function obtenerPendientesResueltosConUserID() {
            sendMessageToServiceWorker({ action: 'obtenerPendientesResueltosConUserID' })
                .then(data => mostrarSalida(data))
                .catch(error => console.error(error));
        }

        function limpiarPantalla() {
            document.getElementById('output').innerHTML = '';
        }

        document.getElementById('obtenerPendientesBtn').addEventListener('click', obtenerPendientes);
        document.getElementById('obtenerPendientesSinResolverBtn').addEventListener('click', obtenerPendientesSinResolver);
        document.getElementById('obtenerPendientesResueltosBtn').addEventListener('click', obtenerPendientesResueltos);
        document.getElementById('obtenerPendientesConUserIDBtn').addEventListener('click', obtenerPendientesConUserID);
        document.getElementById('obtenerPendientesResueltosConUserIDBtn').addEventListener('click', obtenerPendientesResueltosConUserID);
        document.getElementById('limpiarBtn').addEventListener('click', limpiarPantalla);
    }).catch(error => console.error('Error al registrar el Service Worker:', error));
}
